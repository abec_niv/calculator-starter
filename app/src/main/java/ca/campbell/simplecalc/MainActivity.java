package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

public class MainActivity extends Activity {

    EditText etNumber1, etNumber2;
    TextView result;
    double num1, num2, answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    public void clearValues(View v) {
        etNumber1.setText(R.string.empty);
        etNumber2.setText(R.string.empty);
        result.setText(R.string.empty);
    }

    // TODO: replace with code that adds the two input numbers
    public void addNums(View v) {
        num1 = getNum(etNumber1);
        num2 = getNum(etNumber2);
        answer = MathOperation.operate(num1, num2, MathOperation.Operation.ADD);
        displayAnswerOnScreen(answer);
    }  //addNums()

    public void subtractNums(View v) {
        num1 = getNum(etNumber1);
        num2 = getNum(etNumber2);
        answer = MathOperation.operate(num1, num2, MathOperation.Operation.SUBTRACT);
        displayAnswerOnScreen(answer);
    }

    public void multiplyNums(View v) {
        num1 = getNum(etNumber1);
        num2 = getNum(etNumber2);
        answer = MathOperation.operate(num1, num2, MathOperation.Operation.MULTIPLY);
        displayAnswerOnScreen(answer);
    }

    public void divideNums(View v) {
        num1 = getNum(etNumber1);
        num2 = getNum(etNumber2);
        try {
            displayAnswerOnScreen(
                    MathOperation.operate(num1, num2, MathOperation.Operation.DIVIDE));
        } catch (ArithmeticException ae) {
            result.setText(R.string.divideByZero);
        }
    }

    private double getNum(EditText et) {
        return Double.parseDouble(et.getText().toString());
    }

    private void displayAnswerOnScreen(double answer) {
        displayAnswerOnScreen(Double.toString(answer));
    }

    private void displayAnswerOnScreen(String answer) {
        result.setText(answer);
    }
}