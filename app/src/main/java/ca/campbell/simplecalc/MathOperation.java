package ca.campbell.simplecalc;

public class MathOperation {

    double num1, num2;
    public enum Operation {
        ADD,
        SUBTRACT,
        MULTIPLY,
        DIVIDE
    }

    public static double operate(double num1, double num2, Operation o) {
        double temp = 0.0;
        switch(o) {
            case ADD:
                temp = num1 + num2;
                break;
            case SUBTRACT:
                temp = num1 - num2;
                break;
            case MULTIPLY:
                temp = num1 * num2;
                break;
            case DIVIDE:
                if(num2 == 0) {
                    throw new ArithmeticException("Cannot divide by zero");
                }
                temp = num1 / num2;
                break;
        }
        return temp;
    }

}
